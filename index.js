const RosApi = require("node-routeros").RouterOSAPI;

const conn = new RosApi({
    host: "103.138.76.1",
    user: "BIG",
    password: "BIG2019!!!"
});

//for get data name
conn.connect().then(async () => {
    var name = "COBA QUEUE";
    await conn.write([
        "/queue/tree/getall",
        '?name='+name
    ]).then((data) => {
        console.log(data);
        return data;
    }).catch((err) => {
        console.log(err.message);
    });
    conn.close();
});

//delete
conn.connect().then(async () => {
    //add UPLOAD
    await conn.write([
        "/queue/tree/remove",
        "=.id=*1000011"
    ]).then((data) => {
        console.log("Sukses menghapus");
    }).catch((err) => {
        console.log(err.message);
    });

    //add DOWNLOAD
    await conn.write([
        "/queue/tree/remove",
        "=.id=*1000012"
    ]).then((data) => {
        console.log("Sukses menghapus");
    }).catch((err) => {
        console.log(err.message);
    });
    conn.close();
});

//for add data
conn.connect().then(async () => {
    var name = "COBA BIGIO";
    var template_up = name + " UP";
    var template_down = name + " DOWN";
    var parent_up = "Paket 10M 1:8 Up";
    var parent_down = "Paket 10M 1:8 Down";
    var limit_max = "10M";
    var limit_min = "1M";

    //add UPLOAD
    await conn.write([
        "/queue/tree/add",
        "=name=" + template_up,
        "=parent=" + parent_up,
        "=packet-mark=" + template_up,
        "=max-limit=" + limit_max,
        "=limit-at=" + limit_min,
    ]).then((data) => {
        console.log("UPLOAD: " + data[0].ret);
        return data;
    }).catch((err) => {
        console.log(err.message);
    });

    //add DOWNLOAD
    await conn.write([
        "/queue/tree/add",
        "=name=" + template_down,
        "=parent=" + parent_down,
        "=packet-mark=" + template_down,
        "=max-limit=" + limit_max,
        "=limit-at=" + limit_min,
    ]).then((data) => {
        console.log("DOWNLOAD: " + data[0].ret);
        return data;
    }).catch((err) => {
        console.log(err.message);
    });
    conn.close();
});

//edit
conn.connect().then(async () => {
    var name = "COBA BIGIO LAGI";
    var template_up = name + " UP";
    var template_down = name + " DOWN";
    var parent_up = "Coba1_Up";
    var parent_down = "Coba1_Down";
    var limit_max = "5M";
    var limit_min = "2M";

    //edit UPLOAD
    await conn.write([
        "/queue/tree/set",
        "=name=" + template_up,
        "=parent=" + parent_up,
        "=packet-mark=" + template_up,
        "=max-limit=" + limit_max,
        "=limit-at=" + limit_min,
        "=.id=*1000015"
    ]).then((data) => {
        console.log("Sukses Edit");
    }).catch((err) => {
        console.log(err.message);
    });

    //edit DOWNLOAD
    await conn.write([
        "/queue/tree/set",
        "=name=" + template_down,
        "=parent=" + parent_down,
        "=packet-mark=" + template_down,
        "=max-limit=" + limit_max,
        "=limit-at=" + limit_min,
        "=.id=*1000016"
    ]).then((data) => {
        console.log("Sukses Edit");
    }).catch((err) => {
        console.log(err.message);
    });
    conn.close();
});